# Snyk - GitLab Security Dashboard Tool

_better name incoming_

This is a hackathon project from the Snyk SEs (Mike Braun, Nate Michalov, Omar Quimbaya, ). The goal of the project is to create a way to transform output from the Snyk CLI to the GitLab reporting format.

Copyright Snyk 2021.
